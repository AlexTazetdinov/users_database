function processGetRequest(url, headers, data, callback){
	$.ajax({
		url:url,
		headers:headers,
		type:'GET',
		data:data,
		success:callback
	})
}


function processPostRequest(url, headers, data, callback){
	$.ajax({
		url:url,
		headers:headers,
		type:'POST',
		data:data,
		success:callback,
		error:function(){alert("Error");}
	})
}

function processPostRequestForJsonAndFile(url, data, callback){
	$.ajax({
		url:url,
		type:'POST',
		data:data,
		processData : false,
		contentType : false,
		success:callback,
		error:function(){alert("Error");}
	})
}

function processPutRequest(url, headers, data, callback){
	$.ajax({
		url:url,
		headers:headers,
		type:'PUT',
		data:data,
		success:callback,
		error:function(){alert("Error");}
	})
}

function processPutRequestForJsonAndFile(url, data, callback){
	$.ajax({
		url:url,
		type:'PUT',
		data:data,
		processData : false,
		contentType : false,
		success:callback,
		error:function(){alert("Error");}
	})
}

function processDeleteRequest(url, headers, data, callback){
	$.ajax({
		url:url,
		headers:headers,
		type:'DELETE',
		data:data,
		success:callback,
		error:function(){alert("Error");}
	})
}
