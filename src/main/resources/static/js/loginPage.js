function show() {
	$('#modalLogin').modal({
		backdrop : 'static',
		keyboard : false
	});
}

function getDataToSend() {
	var data = {};
	data['login'] = $('#login').val();
	data['password'] = $('#password').val();
	return data;
}

function sendLoginRequest() {
	$("#loginForm").ajaxSubmit({
		url : '/loginProcessing',
		type : 'post',
//		success : function(data,textStatus,request) {
//			handleData(data,textStatus,request);
//		}
		complete:function(jqXHR,status,options){
			if(status==='error'){
				$('#error').html(jqXHR.responseText);
			}
			else {
				var url=jqXHR.getResponseHeader("url");
				window.location.replace(url);}
}
	});
}

//function handleData(data,textStatus,request) {
//	if (textStatus.indexOf("textStatus") === -1)
//		window.location.replace(request.getResponseHeader('url'));
//	else
//		$('#error').html(data);
//}
