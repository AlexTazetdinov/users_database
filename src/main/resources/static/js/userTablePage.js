var defaultHeaders = {
	"Accept" : "application/json; charset=utf-8",
	"Content-Type" : "application/json; charset=utf-8"
};
function UserTable() {
	var thisObject = this;
	function handleResponse(json, emailName, loginName, modal) {
		var message = json.OK.string;
		if (message === "") {
			thisObject.getAndReplaceTable();
			clearFields();
			$(modal).modal('toggle');
		} else {
			if (message.indexOf("email") !== -1) {
				unMark([ emailName ]);
				markAsWarning([ emailName ], $('#existedError').text());
			}
			if (message.indexOf("login") !== -1) {
				unMark([ loginName ]);
				markAsWarning([ loginName ], $('#existedError').text());
			}
		}
	}
	this.openSendEmailWindow = function() {
		clearMailPopup();
		var id = getUserId();
		if (id) {
			$('#modalSendEmail').modal('show');
		} else {
			$('#modalError').modal('show');
		}
	}

	this.openUpdateWindow = function() {
		var id = getUserId();
		if (id) {
			var requiredNames = [ "loginUpdate", "passwordUpdate",
					"emailUpdate" ];
			unMark(requiredNames);
			$('#modalUpdateUser').modal('show');
			autoFilling();
		} else {
			$('#modalError').modal('show');
		}
	}

	function openEmailWindow(receiver) {
		$('#modalSendMail').modal('show');
	}

	this.openAddWindow = function() {
		$('#modalCreateUser').modal('show');
	}

	this.add = function() {
		var names = [ "login", "password", "email", "name", "surname" ];
		var requiredNames = [ "login", "password", "email" ];
		var emptyFields = getEmptyFields(requiredNames);
		var nonEmptyFields = [];
		for (var i = 0; i < requiredNames.length; i++) {
			if ($.inArray(requiredNames[i], emptyFields) == -1) {
				nonEmptyFields.push(requiredNames[i]);
			}
		}
		if (emptyFields.length == 0) {
			unMark(requiredNames);
			sendForAdd(getUserFromFieldsForAdd(names));
		} else {
			unMark(names);
			markAsWarning(emptyFields, $('#emptyFieldError').text());
		}
	}

	this.update = function() {
		var names = [ "loginUpdate", "passwordUpdate", "emailUpdate",
				"nameUpdate", "surnameUpdate" ];
		var requiredNames = [ "loginUpdate", "passwordUpdate", "emailUpdate" ];
		var emptyFields = getEmptyFields(requiredNames);
		var nonEmptyFields = [];
		for (var i = 0; i < requiredNames.length; i++) {
			if ($.inArray(requiredNames[i], emptyFields) == -1) {
				nonEmptyFields.push(requiredNames[i]);
			}
		}
		if (emptyFields.length == 0) {
			sendForUpdate(getUserFromFieldsForUpdate(names));
			unMark(requiredNames);
		} else {
			unMark(nonEmptyFields);
			markAsWarning(emptyFields, $('#emptyFieldError').text());
		}
	}

	this.del = function() {
		var id = getUserId();
		if (id) {
			sendForDelete(id);
		} else {
			$('#modalError').modal('show');
		}

	}

	this.sendEmail = function() {
		var message = getMessageFromFields();
		processPostRequest("/sendEmail", defaultHeaders, JSON
				.stringify(message), function(data) {
			$('#modalSendEmail').modal('toggle');
		});
	}

	this.changeLanguage = function() {
		var lang = $("#languages").val();
		window.location.replace(window.location.pathname + "?lang=" + lang);
	}

	this.setDefaultLanguage = function() {
		var lang = $("#locale").val();
		$("#languages").val(lang);
	}

	function replaceTableHTML(html) {
		$('#userTable').html(html);
		initializeAddressForSingleUser();
	}

	this.getAndReplaceTable = function() {
		processGetRequest("/users", "", null, replaceTableHTML);
	}

	function sendForAdd(user) {
		processPostRequest("/users", defaultHeaders, JSON.stringify(user),
				function(data) {
					handleResponse(data, "email", "login", '#modalCreateUser');
				});
	}

	function sendForUpdate(user) {
		processPutRequest('/users', defaultHeaders, JSON.stringify(user),
				function(data) {
					handleResponse(data, "emailUpdate", "loginUpdate",
							'#modalUpdateUser');
				});
	}

	function sendForDelete(id) {
		processDeleteRequest("/users", defaultHeaders, JSON.stringify(id),
				function(data) {
					thisObject.getAndReplaceTable()
				})
	}
	function handleError(request, status, error) {
		if (request.status === 403) {
			$('#modalNoAccess').modal('show');
		}
	}

	function getUserFromFieldsForUpdate(fieldNames) {
		var user = getUserFromFieldsForAdd(fieldNames);
		user['id'] = $('input[name=checked-user]:checked').val();
		return user;
	}

	function getUserFromFieldsForAdd(fieldNames) {
		var user = {
			login : "" + document.getElementById(fieldNames[0]).value,
			password : "" + document.getElementById(fieldNames[1]).value,
			email : "" + document.getElementById(fieldNames[2]).value,
			name : "" + document.getElementById(fieldNames[3]).value,
			surname : "" + document.getElementById(fieldNames[4]).value,
			role : {
				id : 1
			}
		}
		return user;
	}

	function getMessageFromFields() {
		var id = getUserId();
		var message = {
			recieverId : id,
			subject : $('#subject').val(),
			text : $('#text').val()
		}
		return message;
	}

	function getUserId() {
		var id = +$('input[name=checked-user]:checked').val();
		return id;
	}

	function autoFilling() {
		var table = document.getElementById("userTable");
		var userId = $('input[name=checked-user]:checked').val();
		var names = [ 'login', 'password', 'email', 'name', 'surname' ];
		for (var i = 0; i < names.length; i++) {
			var insert = $('[id="' + names[i] + userId + '"]').text();
			$('[id="' + names[i] + 'Update"]').val(insert);
		}
	}

	function clearFields() {
		var names = [ 'login', 'password', 'email', 'name', 'surname' ];
		for (var i = 0; i < names.length; i++) {
			$('[id="' + names[i] + '"]').val("");
		}
	}
	function clearMailPopup() {
		$('[id=subject]').val("");
		$('[id=text]').val("");
	}
	function getEmptyFields(fieldNames) {
		var result = [];
		for (var i = 0; i < fieldNames.length; i++) {
			if (!($('[id="' + fieldNames[i] + '"]').val())) {
				var name = fieldNames[i];
				result.push(name);
			}
		}
		return result;
	}

	function wrapNonEmptyFields(fieldNames) {
		for (var i = 0; i < fieldNames.length; i++) {
			$('[id="' + fieldNames[i] + '"]').unwrap();
			$('[id="' + fieldNames[i] + '"]').wrap("<p></p>")
		}
	}

	function addGlyphicon(fieldNames) {
		for (var i = 0; i < fieldNames.length; i++) {
			$('[id="' + fieldNames[i] + '"]')
					.after(
							"<span class='glyphicon glyphicon-warning-sign  form-control-feedback'></span>");
		}
	}

	function markAsWarning(fieldNames, message) {
		wrapEmptyFields(fieldNames);
		addGlyphicon(fieldNames);
		addToolTip(fieldNames, message);
		function wrapEmptyFields(fieldNames) {
			for (var i = 0; i < fieldNames.length; i++) {
				$('[id="' + fieldNames[i] + '"]').unwrap();
				$('[id="' + fieldNames[i] + '"]').wrap(
						"<p class='has-error has-feedback'></p>")
			}
		}
	}

	function unMark(fieldNames) {
		removeGlyphicon();
		wrapNonEmptyFields(fieldNames);
		removeToolTip(fieldNames);
		function removeGlyphicon() {
			$(".glyphicon-warning-sign").remove();
		}
	}

	function addToolTip(fieldNames, message) {
		removeToolTip(fieldNames);
		for (var i = 0; i < fieldNames.length; i++) {
			$('[id="' + fieldNames[i] + '"]').attr('data-original-title',
					message).tooltip('fixTitle');
		}
	}

	function removeToolTip(fieldNames) {
		for (var i = 0; i < fieldNames.length; i++) {
			$('[id="' + fieldNames[i] + '"]').attr('data-original-title', '')
					.tooltip('fixTitle')

		}
	}
}

function AddressTable() {
	var addWindowId = "modalCreateAddress";
	var updateWindowId = "modalUpdateAddress";
	var addressTable = this;
	this.targetUrl = '/getAllAddresses';

	this.openUpdateWindow = function() {
		var id = getAddressId();
		if (id) {
			autoFilling();
			$('#modalUpdateAddress').modal('show');
		} else {
			$('#modalError').modal('show');
		}
	}

	this.openAddWindow = function() {
		loadUserSelector();
		$("#" + addWindowId).modal('show');
	}

	this.add = function() {
		var names = [ 'description', 'country', 'region', 'city', 'street',
				'house', 'flat', 'zip' ];
		sendForAdd(getAddressFromFieldsForAdd(names));
	}

	this.update = function() {
		var names = [ 'descriptionUpdate', 'countryUpdate', 'regionUpdate',
				'cityUpdate', 'streetUpdate', 'houseUpdate', 'flatUpdate',
				'zipUpdate' ];
		sendForUpdate(getAddressFromFieldsForUpdate(names));
	}

	this.del = function() {
		var id = getAddressId();
		if (id) {
			sendForDelete(id);
		} else {
			$('#modalError').modal('show');
		}

	}
	this.handleError = function(request, status, error) {
		if (request.status === 403) {
			$('#modalNoAccess').modal('show');
		}
	}

	function changeLanguage() {
		var lang = $("#languages").val();
		window.location.replace(window.location.pathname + "?lang=" + lang);
	}

	function setDefaultLanguage() {
		var lang = $("#locale").val();
		$("#languages").val(lang);
	}
	window.onload = function() {
		setDefaultLanguage();
	}

	this.replaceTableHTML = function(html) {
		$('#addressTable').html(html);
	}

	function sendForAdd(address) {
		processPostRequest("/addresses", defaultHeaders, JSON
				.stringify(address), function(data) {
			addressTable.handleResponse('#modalCreateAddress');
		});
	}

	function sendForUpdate(address) {
		processPutRequest('/addresses', defaultHeaders,
				JSON.stringify(address), function(data) {
					addressTable.handleResponse('#modalUpdateAddress');
				});
	}

	function sendForDelete(id) {
		processDeleteRequest("/addresses", defaultHeaders, JSON.stringify(id),
				function(data) {
					addressTable.getAndReplaceTable()
				})
	}

	function loadUserSelector() {
		processGetRequest("/getUserLogins", "", null, function(data) {
			$('.selectUser').html(data);
		})
	}

	function getAddressFromFieldsForUpdate(fieldNames) {
		var address = getAddressFromFieldsForAdd(fieldNames);
		address['id'] = $('input[name=checked-address]:checked').val();
		address['userId'] = $("#userIdInAddress" + address['id']).val();
		return address;
	}

	function getAddressFromFieldsForAdd(fieldNames) {
		var address = {
			userId : $("#addUserSelector").val(),
			description : "" + document.getElementById(fieldNames[0]).value,
			country : "" + document.getElementById(fieldNames[1]).value,
			region : "" + document.getElementById(fieldNames[2]).value,
			city : "" + document.getElementById(fieldNames[3]).value,
			street : "" + document.getElementById(fieldNames[4]).value,
			house : "" + document.getElementById(fieldNames[5]).value,
			flat : "" + document.getElementById(fieldNames[6]).value,
			zip : "" + document.getElementById(fieldNames[7]).value
		}
		return address;
	}

	function getAddressId() {
		var id = +$('input[name=checked-address]:checked').val();
		return id;
	}

	function autoFilling() {
		var table = document.getElementById("addressTable");
		var addressId = $('input[name=checked-address]:checked').val();
		var names = [ 'description', 'country', 'region', 'city', 'street',
				'house', 'flat', 'zip' ];
		for (var i = 0; i < names.length; i++) {
			var insert = $('[id="' + names[i] + addressId + '"]').text();
			$('[id="' + names[i] + 'Update"]').val(insert);
		}

	}

	function clearFields() {
		var names = [ 'description', 'country', 'region', 'city', 'street',
				'house', 'flat', 'zip', ];
		for (var i = 0; i < names.length; i++) {
			$('[id="' + names[i] + '"]').val();
		}
	}
}
AddressTable.prototype.getAndReplaceTable = function() {
	var addressTable = this;
	processGetRequest(addressTable.targetUrl, "", null, function(data) {
		addressTable.replaceTableHTML(data)
	})
}

AddressTable.prototype.getUserId = function(addressId) {
	var id = +$("#userIdInAddress" + addressId).val();
	return id;
}
AddressTable.prototype.handleResponse = function(modal) {
	this.getAndReplaceTable();
	$(modal).modal('toggle');
}

function AddressTableForSingleUser(userId) {
	AddressTable.apply(this, arguments);
	this.userId = userId;
	this.targetUrl = "/addressesForSingleUser";
	this.addressTable = this;
}
AddressTableForSingleUser.prototype = Object.create(AddressTable.prototype);
AddressTableForSingleUser.prototype.getUserId = function() {
	return this.userId;
}
AddressTableForSingleUser.prototype.getAndReplaceTable = function() {
	var addressTableForSingleUser = this;
	var userId = addressTableForSingleUser.userId;
	processGetRequest(addressTableForSingleUser.targetUrl, "", 'userId='
			+ userId, function(data) {
		addressTableForSingleUser.replaceTableHTML(data)
	})
}
function MoviesTable() {
	var moviesTable = this;
	this.getAndReplaceTable = function() {
		processGetRequest("/movie", "", null, function(data) {
			moviesTable.replaceTableHTML(data);
		});
		this.openUpdateWindow = function() {
			var id = getMovieId();
			if (id) {
				loadAndReplaceGenreList();
				$('#modalUpdateMovie').modal('show');
			} else {
				$('#modalError').modal('show');
			}
		}

		this.openAddWindow = function() {
			$("#modalAddMovie").modal('show');
			loadAndReplaceGenreList();
		}

		this.add = function() {
			var names = [ 'movieNameAdd', 'movieGenresSelector', 'movieLinkAdd' ];
			var data = getMovieFromFieldsForAdd(names);
			sendForAdd(data);
		}

		this.update = function() {
			var names = [ 'movieNameUpdate', 'movieGenresUpdate',
					'movieLinkUpdate' ];
			sendForUpdate(getMovieFromFieldsForUpdate(names));
		}

		this.del = function() {
			var id = getMovieId();
			if (id) {
				sendForDelete(id);
			} else {
				$('#modalError').modal('show');
			}

		}
		this.handleResponse = function(modal) {
			clearFields();
			this.getAndReplaceTable();
			$(modal).modal('toggle');
		}
		this.handleError = function(request, status, error) {
			if (request.status === 403) {
				$('#modalNoAccess').modal('show');
			}
		}

		function clearFields() {
			$(':input').val('');
		}

		function loadAndReplaceGenreList() {
			processGetRequest("/genresAsSelectBox", "", null, replace)
			function replace(data) {
				$("#movieGenresSelectorAdd").html(data);
				$("#movieGenresSelectorUpdate").html(data);
				$('.selectpicker').selectpicker('refresh');
				$('.selectpicker').selectpicker('render');
				autoFilling();
			}
		}

		function changeLanguage() {
			var lang = $("#languages").val();
			window.location.replace(window.location.pathname + "?lang=" + lang);
		}

		function setDefaultLanguage() {
			var lang = $("#locale").val();
			$("#languages").val(lang);
		}
		window.onload = function() {
			setDefaultLanguage();
		}

		this.replaceTableHTML = function(html) {
			$('#moviesTable').html(html);
			initializeComments(moviesTable);
		}

		function sendForAdd(movie) {
			processPostRequestForJsonAndFile("/movie", movie, function(data) {
				moviesTable.handleResponse('#modalAddMovie');
			});
		}

		function sendForUpdate(movie) {
			processPutRequestForJsonAndFile("/movie", movie, function(data) {
				moviesTable.handleResponse('#modalUpdateMovie');
			});
		}

		function sendForDelete(id) {
			processDeleteRequest("/movie", defaultHeaders, JSON.stringify(id),
					function(data) {
						moviesTable.getAndReplaceTable()
					})
		}

		this.getComments = function(movieId) {
			processGetRequest("/commentaries", "", 'movieId=' + movieId,
					function(data) {
						$('body').html(data);
						addListenersToComments(moviesTable);
					})
		}

		function getMovieFromFieldsForUpdate(fieldNames) {
			var formData = new FormData();
			var movie = {
				name : "" + document.getElementById(fieldNames[0]).value,
				genresArray : $("#movieGenresSelectorUpdate").val().map(Number),
				link : "" + document.getElementById(fieldNames[2]).value,
				id : $('input[name=checked-movie]:checked').val()
			}
			var fileData = $('#moviePictureUpdate').prop('files')[0];
			formData.append("image", fileData);
			formData.append("movie", new Blob([ JSON.stringify(movie) ], {
				type : 'application/json'
			}));
			return formData;
		}

		function getMovieFromFieldsForAdd(fieldNames) {
			var formData = new FormData();
			var movie = {
				name : '' + document.getElementById(fieldNames[0]).value,
				genresArray : $("#movieGenresSelectorAdd").val().map(Number),
				link : '' + document.getElementById(fieldNames[2]).value,
			}
			var fileData = $('#moviePictureAdd').prop('files')[0];
			formData.append("image", fileData);
			formData.append("movie", new Blob([ JSON.stringify(movie) ], {
				type : 'application/json'
			}));
			return formData;
		}
		function getMovieId() {
			var id = +$('input[name=checked-movie]:checked').val();
			return id;
		}

		function autoFilling() {
			var movieId = $('input[name=checked-movie]:checked').val();
			var names = [ 'movieName', 'movieLink' ];
			for (var i = 0; i < names.length; i++) {
				var insert = $('[id="' + names[i] + movieId + '"]').text();
				$('[id="' + names[i] + 'Update"]').val(insert);
			}
			var genreNames = $('[id="' + "movieGenre" + movieId + '"]').text()
					.split(",");
			var genreIds = [];
			genreNames
					.forEach(function(item, i, genreNames) {
						var itemEntity = $("#movieGenresSelectorUpdate >option:contains('"
								+ item + "')");
						var value = itemEntity.val();
						genreIds.push(value);
					});
			$('#movieGenresSelectorUpdate').selectpicker('val', genreIds);
		}
	}
}

function GenresTable() {
	var genresTable = this;
	this.getAndReplaceTable = function() {
		processGetRequest("/genres", "", null, function(data) {
			genresTable.replaceTableHTML(data);
		})
		this.openUpdateWindow = function() {
			var id = getGenreId();
			if (id) {
				autoFilling();
				$('#modalUpdateGenre').modal('show');
			} else {
				$('#modalError').modal('show');
			}
		}

		this.openAddWindow = function() {
			$("#modalAddGenre").modal('show');
		}

		this.add = function() {
			var names = [ 'addGenreName' ];
			sendForAdd(getGenreFromFieldsForAdd(names));
		}

		this.update = function() {
			var names = [ 'genreNameUpdate' ];
			sendForUpdate(getGenreFromFieldsForUpdate(names));
		}

		this.del = function() {
			var id = getGenreId();
			if (id) {
				sendForDelete(id);
			} else {
				$('#modalError').modal('show');
			}

		}
		this.handleResponse = function(modal) {
			genresTable.getAndReplaceTable();
			clearFields();
			$(modal).modal('toggle');
		}
		this.handleError = function(request, status, error) {
			if (request.status === 403) {
				$('#modalNoAccess').modal('show');
			}
		}
		function clearFields() {
			$(':input').val('');
		}

		function changeLanguage() {
			var lang = $("#languages").val();
			window.location.replace(window.location.pathname + "?lang=" + lang);
		}

		function setDefaultLanguage() {
			var lang = $("#locale").val();
			$("#languages").val(lang);
		}
		window.onload = function() {
			setDefaultLanguage();
		}

		this.replaceTableHTML = function(html) {
			$('#genresTable').html(html);
		}

		function sendForAdd(genre) {
			processPostRequest("/genres", defaultHeaders,
					JSON.stringify(genre), function(data) {
						genresTable.handleResponse('#modalAddGenre');
					});
		}

		function sendForUpdate(genre) {
			processPutRequest('genres', defaultHeaders, JSON.stringify(genre),
					function(data) {
						genresTable.handleResponse('#modalUpdateGenre');
					})
		}

		function sendForDelete(id) {
			processDeleteRequest("/genres", defaultHeaders, JSON.stringify(id),
					function(data) {
						genresTable.getAndReplaceTable()
					})
		}

		function getGenreFromFieldsForUpdate(fieldNames) {
			var genre = getGenreFromFieldsForAdd(fieldNames);
			genre['id'] = $('input[name=checked-genre]:checked').val();
			return genre;
		}

		function getGenreFromFieldsForAdd(fieldNames) {
			var genre = {
				name : "" + document.getElementById(fieldNames[0]).value,
			}
			return genre;
		}
		function getGenreId() {
			var id = +$('input[name=checked-genre]:checked').val();
			return id;
		}

		function autoFilling() {
			var table = document.getElementById("genreTable");
			var genreId = getGenreId();
			var names = [ 'genreName' ];
			for (var i = 0; i < names.length; i++) {
				var insert = $('[id="' + names[i] + genreId + '"]').text();
				$('[id="' + names[i] + 'Update"]').val(insert);
			}

		}

		function clearFields() {
			var names = [ 'description', 'country', 'region', 'city', 'street',
					'house', 'flat', 'zip', ];
			for (var i = 0; i < names.length; i++) {
				$('[id="' + names[i] + '"]').val();
			}
		}
	}
}

function main() {
	var userTable = new UserTable();
	var addressTable = new AddressTable();
	var moviesTable = new MoviesTable();
	var genresTable = new GenresTable();
	userTable.getAndReplaceTable();
	$('#usersTab').click(function() {
		userTable.getAndReplaceTable();
		$('#userTab').tab('show');
	});
	$('#addressTab').click(function() {
		addressTable.getAndReplaceTable();
		addListenersToAddresses(addressTable);
		$('#addressTab').tab('show');
	});
	$('#moviesTab').click(function() {
		moviesTable.getAndReplaceTable();
		addListenersToMovies(moviesTable);
		$('#moviesTab').tab('show');
	});
	$('#genresTab').click(function() {
		genresTable.getAndReplaceTable();
		addListenersToGenres(genresTable);
		$('#genresTab').tab('show');
	});
	addListenersToUsers(userTable);


	$('#languages').change(function() {
		changeLanguage();
	})
	setDefaultLanguage();
}

function addListenersToUsers(userTable) {
	$('#buttonOpenAddUser').click(function() {
		userTable.openAddWindow();
	})
	$('#buttonAddUser').click(function() {
		userTable.add();
	})
	$('#buttonOpenUpdateUser').click(function() {
		userTable.openUpdateWindow();
	})
	$('#buttonUpdateUser').click(function() {
		userTable.update();
	})

	$('#buttonOpenSendEmail').click(function() {
		userTable.openSendEmailWindow();
	})
	$('#buttonOpenSendEmail').click(function() {
		userTable.openSendEmailWindow();
	})
	$('#buttonDeleteUser').click(function() {
		userTable.del();
	})
}

function addListenersToAddresses(addressTable) {
	$('#buttonOpenAddAddress').unbind();
	$('#buttonAddAddress').unbind();
	$('#buttonOpenUpdateAddress').unbind();
	$('#buttonUpdateAddress').unbind();
	$('#buttonDeleteAddress').unbind();
	$('#buttonOpenAddAddress').click(function() {
		addressTable.openAddWindow();
	})
	$('#buttonAddAddress').click(function() {
		addressTable.add();
	})

	$('#buttonOpenUpdateAddress').click(function() {
		addressTable.openUpdateWindow();
	})
	$('#buttonUpdateAddress').click(function() {
		addressTable.update();
	})

	$('#buttonDeleteAddress').click(function() {
		addressTable.del();
	})
}
function addListenersToMovies(moviesTable) {
	$('#buttonOpenAddMovie').unbind();
	$('#buttonOpenAddMovie').click(function() {
		moviesTable.openAddWindow();
	})
	$('#buttonAddMovie').unbind();
	$('#buttonAddMovie').click(function() {
		moviesTable.add();
	})
	$('#buttonOpenUpdateMovie').unbind();
	$('#buttonOpenUpdateMovie').click(function() {
		moviesTable.openUpdateWindow();
	})
	$('#buttonUpdateMovie').unbind();
	$('#buttonUpdateMovie').click(function() {
		moviesTable.update();
	})
	$('#buttonDeleteMovie').unbind();
	$('#buttonDeleteMovie').click(function() {
		moviesTable.del();
	})
}

function addListenersToGenres(genresTable) {
	$('#buttonOpenAddGenre').unbind();
	$('#buttonAddGenre').unbind();
	$('#buttonUpdateGenre').unbind();
	$('#buttonOpenUpdateGenre').unbind();
	$('#buttonDeleteGenre').unbind();
	$('#buttonOpenAddGenre').click(function() {
		genresTable.openAddWindow();
	})
	$('#buttonAddGenre').click(function() {
		genresTable.add();
	})
	$('#buttonOpenUpdateGenre').click(function() {
		genresTable.openUpdateWindow();
	})
	$('#buttonUpdateGenre').click(function() {
		genresTable.update();
	})
	$('#buttonDeleteGenre').click(function() {
		genresTable.del();
	})
}

function getCommentFromFields() {
	var text = $('#commentText').val().replace(/\n\r?/g, '<br />');
	var comment = {
		text : text,
		movieId : $('#movieIdInComments').val()
	};

	return comment;
}

function addListenersToComments(moviesTable) {
	$('#btnAddComment').click(
			function() {
				var comment = getCommentFromFields();
				processPostRequest("/commentaries", defaultHeaders, JSON
						.stringify(comment), function(data) {
					moviesTable.getComments($('#movieIdInComments').val());
				})
			})
	$('#btnGoToMoviesTab').click(
			function() {
				processGetRequest("/", "", null, function(data) {
					var html = data.slice(data.indexOf("<body>"), data
							.lastIndexOf("</body>"));
					$("body").html(html);
					moviesTable.getAndReplaceTable();
					addListenersToMovies(moviesTable);
					$('#moviesTab').tab('show');
				})
			});
}
function changeLanguage() {
	var lang = $("#languages").val();
	window.location.replace(window.location.pathname + "?lang=" + lang);
}

function setDefaultLanguage() {
	var lang = $("#locale").val();
	$("#languages").val(lang);
}

function initializeAddressForSingleUser() {
	$(".addresses-for-single-user-link").click(function() {
		var addressTableForSingleUser = new AddressTableForSingleUser(this.id);
		addListenersToAddresses(addressTableForSingleUser);
		addressTableForSingleUser.getAndReplaceTable();
		addListenersToAddresses(addressTableForSingleUser);
		$("#addressTab").tab("show");
	});
}
function initializeComments(movieTable) {
	$(".comments-for-movie").click(function() {
		movieTable.getComments(this.id);
	});
}
