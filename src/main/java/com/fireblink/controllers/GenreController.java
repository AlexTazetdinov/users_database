package com.fireblink.controllers;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fireblink.beans.CustomAddress;
import com.fireblink.beans.CustomGenre;
import com.fireblink.models.Genre;
import com.fireblink.services.GenreService;

@Controller
@EnableAutoConfiguration
public class GenreController {
	@Autowired
	private GenreService genreService;

	@Secured("ROLE_USER")
	@RequestMapping(value = "/genres", method = { RequestMethod.GET })
	public ModelAndView showAllGenres(@RequestParam(value = "asSelectBox", required = false) boolean asSelectBox) {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("genres", genreService.getAll());
		if (asSelectBox)
			return new ModelAndView("genreList", modelMap);
		else
			return new ModelAndView("genres", modelMap);
	}

	@RequestMapping(value = "/genres", method = RequestMethod.PUT)
	public @ResponseBody JsonObject update(@RequestBody CustomGenre customGenre) {
		genreService.update(customGenre.getGenre(genreService));
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
		objectBuilder.add(HttpStatus.OK.name(), "");
		return objectBuilder.build();
	}

	@RequestMapping(value = "/genres", method = RequestMethod.POST)
	public @ResponseBody JsonObject add(@RequestBody Genre genre) {
		genreService.add(genre);
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
		objectBuilder.add(HttpStatus.OK.name(), "");
		return objectBuilder.build();
	}

	@RequestMapping(value = "/genres", method = RequestMethod.DELETE)
	public @ResponseBody JsonObject delete(@RequestBody Integer id) {
		genreService.deleteById(id);
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
		objectBuilder.add(HttpStatus.OK.name(), "");
		return objectBuilder.build();
	}
	
	@Secured("ROLE_USER")
	@RequestMapping(value = "/genresAsSelectBox", method = { RequestMethod.GET })
	public ModelAndView showGenresAsSelectBox() {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("genres", genreService.getAll());
		return new ModelAndView("genreList", modelMap);
	}
}
