package com.fireblink.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fireblink.beans.CustomCommentary;
import com.fireblink.beans.CustomMovie;
import com.fireblink.configs.FileConfig;
import com.fireblink.models.Commentary;
import com.fireblink.models.Movie;
import com.fireblink.models.User;
import com.fireblink.services.CommentaryService;
import com.fireblink.services.GenreService;
import com.fireblink.services.MovieService;
import com.fireblink.services.UserService;

@Controller
@EnableAutoConfiguration
//@Import(MultipartAutoConfiguration.class)
public class MovieController {
	@Autowired
	MovieService movieService;
	@Autowired
	UserService userService;
	@Autowired
	CommentaryService commentaryService;
	@Autowired
	GenreService genreService;

	@Secured({ "ROLE_USER", "ROLE_ADMIN" })
	@RequestMapping(value = "/movie", method = { RequestMethod.GET })
	public ModelAndView getAllMovies() {
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("movies", movieService.getAll());
		return new ModelAndView("movies", modelMap);
	}
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/movie", method = { RequestMethod.POST },consumes = {"multipart/form-data"})
	@ResponseBody
	public ResponseEntity<String> add(@RequestPart("movie") CustomMovie customMovie, @RequestPart(name="image",required=false)MultipartFile image) {
		customMovie.setCustomImage(image);
		movieService.add(customMovie.getMovie(movieService,genreService));
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/movie", method = { RequestMethod.PUT },consumes = {"multipart/form-data"})
	@ResponseBody
	public ResponseEntity<String> update(@RequestPart("movie") CustomMovie customMovie, @RequestPart(name="image",required=false)MultipartFile image) {
		customMovie.setCustomImage(image);
		movieService.update(customMovie.getMovie(movieService,genreService));
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/movie", method = { RequestMethod.DELETE })
	public ResponseEntity<String> delete(@RequestBody Integer movieId) {
		movieService.deleteById(movieId);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

}
