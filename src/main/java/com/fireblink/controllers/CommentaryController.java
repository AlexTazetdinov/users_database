package com.fireblink.controllers;

import java.util.ArrayList;
import java.util.Comparator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fireblink.beans.CustomCommentary;
import com.fireblink.models.Commentary;
import com.fireblink.models.User;
import com.fireblink.services.CommentaryService;
import com.fireblink.services.MovieService;
import com.fireblink.services.UserService;

@Controller
@EnableAutoConfiguration
public class CommentaryController {
	
	@Autowired
	MovieService movieService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	CommentaryService commentaryService;
	
	@Secured({ "ROLE_USER" })
	@RequestMapping(value = "/commentaries", method = { RequestMethod.GET })
	public ModelAndView getCommentsForMovie(@RequestParam(value = "movieId") Integer movieId) {
		ModelMap modelMap = new ModelMap();
		ArrayList<Commentary> comments = new ArrayList<Commentary>();
		comments.addAll(movieService.getById(movieId).getCommentaries());
		comments.sort(new Comparator<Commentary>() {
			@Override
			public int compare(Commentary c1, Commentary c2) {
				return c1.getDateTime().compareTo(c2.getDateTime());
			}
		});
		modelMap.addAttribute("comments", comments);
		modelMap.addAttribute("movie", movieService.getById(movieId));
		return new ModelAndView("comments", modelMap);
	}

	@Secured({ "ROLE_USER" })
	@RequestMapping(value = "/commentaries", method = { RequestMethod.POST })
	public ResponseEntity<String> addComment(@RequestBody CustomCommentary customCommentary) {
		Commentary commentary = customCommentary.getCommentary(movieService);
		Integer currentUserId = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
		commentary.setUser(userService.getById(currentUserId));
		commentaryService.add(commentary);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
}
