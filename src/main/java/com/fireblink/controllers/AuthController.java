package com.fireblink.controllers;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@EnableAutoConfiguration
public class AuthController {

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, Locale locale) {
		ModelAndView modelAndView;
		if (error != null) {
			modelAndView = new ModelAndView("loginError");
			modelAndView.setStatus(HttpStatus.UNAUTHORIZED);
			String errorMessage = messageSource.getMessage("auth.error", null, locale);
			modelAndView.addObject("error", errorMessage);
		} else
			modelAndView = new ModelAndView("login");
		return modelAndView;
	}

	@Secured("ROLE_USER")
	@RequestMapping("/loginSuccess")
	public ResponseEntity<String> loginSuccess() {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("url", "/");
		return new ResponseEntity<String>("Success", responseHeaders, HttpStatus.OK);
	}

}
