package com.fireblink.controllers;

import java.util.ArrayList; 
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.*;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import com.fireblink.beans.CustomUser;
import com.fireblink.dao.UserDao;
import com.fireblink.models.Role;
import com.fireblink.models.User;
import com.fireblink.services.GroupService;
import com.fireblink.services.MailService;
import com.fireblink.services.UserService;
import static com.fireblink.security.SecurityRules.*;;

/**
 * Class UserController
 */
@Controller
@EnableAutoConfiguration
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private GroupService groupService;

	@Secured({"ROLE_USER","ROLE_ADMIN"})
	@RequestMapping(value = "/", method = { RequestMethod.GET })
	public ModelAndView showMainPage() {
		return new ModelAndView("mainPage");
	}
	@Secured({"ROLE_USER","ROLE_ADMIN"})
	@RequestMapping(value = "/users", method = { RequestMethod.GET })
	public ModelAndView showAllUsers(@RequestParam(value = "getAllUsers", required = false) String partOfPage) {
			return getModelAndViewForUser("users");
	}


	@RequestMapping(value = "/users", method = RequestMethod.PUT)
	public @ResponseBody JsonObject updateUser(@RequestBody CustomUser customUser) {
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
		User user = customUser.getUserForUpdate(groupService);
		String message = userService.getMessageAboutExistedFields(user);
		if (message.equals(""))
			userService.update(user);
		objectBuilder.add(HttpStatus.OK.name(), message);
		return objectBuilder.build();
	}


	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public @ResponseBody JsonObject addUser(@RequestBody CustomUser customUser) {
		User user = customUser.getUserForCreate(groupService);
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
		String message = userService.getMessageAboutExistedFields(user);
		if (message.equals(""))
			userService.add(user);
		objectBuilder.add(HttpStatus.OK.name(), message);
		return objectBuilder.build();
	}

	@RequestMapping(value = "/users", method = RequestMethod.DELETE)
	public @ResponseBody ResponseEntity<String> deleteUser(@RequestBody Integer id) {
		userService.delete(userService.getById(id));
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@Secured({"ROLE_USER"})
	@RequestMapping(value = "/getUserLogins", method = { RequestMethod.GET })
	public ModelAndView getUserLogins() {
		return getModelAndViewForUser("userLogins");
	}

	private ModelAndView getModelAndViewForUser(String pageName) {
		List<User> users = new ArrayList();
		try {
			users = userService.getAll();
		} catch (Exception ex) {
		}
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("users", users);
		Map<Integer, String> roles=new HashMap<>();
		for(User user:users){
			Set<Role> role=user.getGroup().getRoles();
			roles.put(user.getId(), StringUtils.collectionToCommaDelimitedString(role));
		}
		modelMap.addAttribute("roles",roles);
		return new ModelAndView(pageName, modelMap);
	}

}
