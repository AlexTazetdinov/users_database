package com.fireblink.controllers;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fireblink.services.ImageService;

@Controller
@EnableAutoConfiguration
public class ImageController {
	@Autowired
	ImageService imageService;


	@RequestMapping(value = "/image/{imageId}",method=RequestMethod.GET)
	@ResponseBody
	public byte[] showImage(@PathVariable Integer imageId) {
		byte[] result = null;
		if (imageId != null)
			result = imageService.getById(imageId).getContent();
		return result;
	}

	@RequestMapping(value = "/image",method=RequestMethod.GET)
	@ResponseBody
	public ModelAndView showNoImage() {
		String redirectUrl="/images/no_image.jpg";
		return  new ModelAndView("redirect:"+redirectUrl);
	}
}
