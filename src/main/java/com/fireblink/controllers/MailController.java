package com.fireblink.controllers;

import java.security.Principal;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fireblink.beans.CustomMessage;
import com.fireblink.services.MailService;
import com.fireblink.services.UserService;
@Controller
@EnableAutoConfiguration
public class MailController {
	
	@Autowired
	private MailService mailService;
	
	@Autowired
	private UserService userService;
	
	@Secured("ROLE_USER")
	@RequestMapping(value = "/sendEmail", method = {RequestMethod.POST})
	public @ResponseBody JsonObject sendEmail(@RequestBody CustomMessage message,Principal principal){
		SimpleMailMessage messageToSend =new SimpleMailMessage();
		mailService.sendMail(parseCustomToSimple(message, principal.getName()));
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder();		
		objectBuilder.add(HttpStatus.OK.name(),"OK");
		return objectBuilder.build();
	}
	
	private SimpleMailMessage parseCustomToSimple(CustomMessage message, String sender){
		SimpleMailMessage result=new SimpleMailMessage();
		result.setFrom(userService.getByLogin(sender).getEmail());
		result.setTo(userService.getById(message.getRecieverId()).getEmail());
		result.setSubject(message.getSubject());
		result.setText(message.getText());
		return result;
	}
}
