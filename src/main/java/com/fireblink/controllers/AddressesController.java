package com.fireblink.controllers;

import static com.fireblink.security.SecurityRules.*;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fireblink.beans.CustomAddress;
import com.fireblink.models.Address;
import com.fireblink.models.User;
import com.fireblink.services.AddressService;
import com.fireblink.services.UserService;

@Controller
@EnableAutoConfiguration
public class AddressesController {
	@Autowired
	private UserService userService;
	@Autowired
	private AddressService addressService;

	@Secured("ROLE_USER")
	@RequestMapping(value = "/addressesForSingleUser", method = { RequestMethod.GET })
	public ModelAndView showAddressesForUser(@RequestParam(value="userId") Integer userId) {
			return getModelAndViewForAddresses(userId);
	}
	
	@Secured("ROLE_USER")
	@RequestMapping(value = "/getAllAddresses", method = { RequestMethod.GET })
	public ModelAndView showAllAddresses() {
			return getModelAndViewForAddresses();
	}

	@RequestMapping(value = "/addresses", method = RequestMethod.PUT)
	public @ResponseBody JsonObject updateAddress(@RequestBody CustomAddress customAddress) {
		addressService.update(customAddress.getAddress(addressService,userService));
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
		objectBuilder.add(HttpStatus.OK.name(), "");
		return objectBuilder.build();
	}

	@RequestMapping(value = "/addresses", method = RequestMethod.POST)
	public @ResponseBody JsonObject addAddress(@RequestBody CustomAddress customAddress) {
		addressService.add(customAddress.getAddress(addressService,userService));
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
		objectBuilder.add(HttpStatus.OK.name(), "");
		return objectBuilder.build();
	}

	@RequestMapping(value = "/addresses", method = RequestMethod.DELETE)
	public @ResponseBody JsonObject deleteAddress(@RequestBody Integer id) {
		addressService.delete(addressService.getById(id));
		JsonObjectBuilder objectBuilder = Json.createObjectBuilder();
		objectBuilder.add(HttpStatus.OK.name(), "");
		return objectBuilder.build();
	}

	private ModelAndView getModelAndViewForAddresses(int userId) {
		List<Address> addresses = new ArrayList();
		try {
			addresses.addAll(userService.getById(userId).getAddresses());
		} catch (Exception ex) {
		}
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("addresses", addresses);
		return new ModelAndView("addressesForSingleUser", modelMap);
	}
	private ModelAndView getModelAndViewForAddresses() {
		List<Address> addresses = new ArrayList();
		try {
			addresses = addressService.getAll();
		} catch (Exception ex) {
		}
		ModelMap modelMap = new ModelMap();
		modelMap.addAttribute("addresses", addresses);
		return new ModelAndView("addressesForAllUsers", modelMap);
	}

}
