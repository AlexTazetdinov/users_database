package com.fireblink.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fireblink.dao.GenreDao;
import com.fireblink.dao.MovieDao;
import com.fireblink.models.Genre;
import com.fireblink.models.Movie;
import com.fireblink.models.User;

@Service
public class MovieService {
	@Autowired
	MovieDao movieDao;
	@Autowired
	GenreDao genreDao;

	public Movie getById(Integer id) {
		return movieDao.getById(id);
	}

	public List<Movie> getAll() {
		return movieDao.getAll();
	}

	public void add(Movie movie) {
		//clarifyGenresInMovie(movie);
		movieDao.create(movie);
	}

	public void delete(Movie movie) {
//		Set<Genre> genres=new HashSet<Genre>(movie.getGenres());
		movieDao.delete(movie);
//		clearGenresWithNoRelations(genres);
	}
	
	public void update(Movie movie){
		movieDao.update(movie);
	}

	public void deleteById(Integer id) {
		delete(movieDao.getById(id));
	}

	private void clarifyGenresInMovie(Movie movie) {
		for (Genre genre : movie.getGenres()) {
			Genre genreInDb = genreDao.getByName(genre.getName());
			if (genreInDb != null) {
				genre.setId(genreInDb.getId());
			}
		}
	}
	private void clearGenresWithNoRelations(Set<Genre>genres){
		for(Genre genre:genres){
			Genre genreInDb=genreDao.getById(genre.getId());
			if(genreInDb.getMovies().size()==0){
				genreDao.delete(genreInDb);
			}
		}
	}
}