package com.fireblink.services;

import static com.fireblink.security.SecurityRules.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.fireblink.dao.AddressDao;
import com.fireblink.dao.UserDao;
import com.fireblink.models.Address;
import com.fireblink.models.User;
import com.fireblink.security.CustomUserDetails;

@Service("addressService")
public class AddressService {
	@Autowired
	private UserDao userDao;
	@Autowired
	private AddressDao addressDao;

	public Address getById(Integer id) {
		return addressDao.getById(id);
	}

	public List<Address> getAll() {
		return addressDao.getAll();
	}
	@PreAuthorize(ADDRESS_ALLOWED_FOR_OWNER_OR_ADMIN)
	public void update(Address address) {
		addressDao.update(address);
	}
	@PreAuthorize(ADDRESS_ALLOWED_FOR_OWNER_OR_ADMIN)
	public void add(Address address) {
		addressDao.create(address);
	}
	@PreAuthorize(ADDRESS_ALLOWED_FOR_OWNER_OR_ADMIN)
	public void delete(Address address) {
		addressDao.delete(address);
	}

	public void addAddressToUser(Address address, Integer userId) {
		address.setUser(userDao.getById(userId));
		addressDao.create(address);
	}
}
