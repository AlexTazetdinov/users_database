package com.fireblink.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fireblink.dao.ImageDao;
import com.fireblink.models.Image;

@Service("imageService")
public class ImageService {
@Autowired
ImageDao imageDao;
public Image getById(Integer id){
	return imageDao.getById(id);
}

}
