package com.fireblink.services;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fireblink.dao.GenreDao;
import com.fireblink.models.Genre;
import com.fireblink.models.Movie;

@Service
public class GenreService {
@Autowired
private GenreDao genreDao;

	public Genre getById(Integer id) {
		return genreDao.getById(id);
	}

	public List<Genre> getAll() {
		return genreDao.getAll();
	}

	public void add(Genre genre) {
		genreDao.create(genre);
	}

	public void delete(Genre genre) {
		genreDao.delete(genre);
	}
	
	public void update(Genre genre){
		genreDao.update(genre);
	}

	public void deleteById(Integer id) {
		delete(genreDao.getById(id));
	}


}
