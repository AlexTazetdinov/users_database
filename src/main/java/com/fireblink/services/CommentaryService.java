package com.fireblink.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fireblink.dao.CommentaryDao;
import com.fireblink.models.Commentary;

@Service
public class CommentaryService {
@Autowired
CommentaryDao commentaryDao;

public Commentary getById(Integer id){
	return commentaryDao.getById(id);
}
public void add(Commentary commentary){
	commentaryDao.create(commentary);
}
}
