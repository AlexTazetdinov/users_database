package com.fireblink.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fireblink.dao.GroupDao;
import com.fireblink.dao.RoleDao;
import com.fireblink.dao.UserDao;
import com.fireblink.models.Role;

@Service("roleService")
public class RoleService {
	@Autowired
	private UserDao userDao;
	
	public List<Role> getRolesByLogin(String login){
		List<Role>roles=new ArrayList();
		roles.addAll(userDao.getByLogin(login).getGroup().getRoles());
		return roles;
	}
	public List<Role>getRolesById(Integer id){
		List<Role>roles=new ArrayList();
		roles.addAll(userDao.getById(id).getGroup().getRoles());
		return roles;
	}
	
}
