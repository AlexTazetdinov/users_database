package com.fireblink.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fireblink.dao.AddressDao;
import com.fireblink.dao.GroupDao;
import com.fireblink.dao.UserDao;
import com.fireblink.models.Address;
import com.fireblink.models.Group;

@Service("groupService")
public class GroupService {
	
		@Autowired
		private GroupDao groupDao;

		@Autowired
		private UserDao userDao;
		
		public Group getById(Integer id){
			return groupDao.getById(id);
		}
		
		public Group getByUserId(Integer id){
			return userDao.getById(id).getGroup();
		}
		
		public List<Group> getAll(){
			return groupDao.getAll();
		}
}
