package com.fireblink.services;

import static com.fireblink.security.SecurityRules.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import com.fireblink.dao.AddressDao;
import com.fireblink.dao.UserDao;
import com.fireblink.models.User;
import com.fireblink.security.CustomUserDetails;

@Service("userService")
public class UserService {

	@Autowired
	private UserDao userDao;
	@Autowired
	private AddressDao addressDao;

	private static String LOGIN_NAME = "login";
	private static String EMAIL_NAME = "email";

	public User getById(int id) {
		return userDao.getById(id);
	}

	public List<User> getAll() {
		return userDao.getAll();
	}

	@PreAuthorize(USER_ALLOWED_FOR_OWNER_OR_ADMIN)
	public void delete(User user) {
		userDao.delete(user);
	}

	@Secured({ "ROLE_ADMIN" })
	public void add(User user) {
		userDao.create(user);
	}

	public boolean checkIfUserHasPermission(User targetUser, CustomUserDetails currentUser) {
		boolean result;
		if (currentUser.getAuthorities().stream().anyMatch(x -> x.getAuthority() == "ROLE_ADMIN")) {
			result = true;
		} else if (targetUser.getId().equals(currentUser.getId())) {
			result = true;
		} else
			result = false;
		return result;
	}

	public String getMessageAboutExistedFields(User user) {
		String result = "";
		if (checkIfLoginExist(user))
			result += LOGIN_NAME + " ";
		if (checkIfEmailExist(user))
			result += EMAIL_NAME + " ";
		return result;
	}

	private boolean checkIfLoginExist(User user) {
		User userFromDb;
		userFromDb = userDao.getByLogin(user.getLogin());
		boolean result;
		if (userFromDb == null) {
			result = false;
		} else
			result = !(userFromDb.getId().equals(user.getId()));
		return result;
	}

	private boolean checkIfEmailExist(User user) {
		User userFromDb;
		userFromDb = userDao.getByEmail(user.getEmail());
		boolean result;
		if (userFromDb == null) {
			result = false;
		} else
			result = !(userFromDb.getId().equals(user.getId()));
		return result;
	}

	@PreAuthorize(USER_ALLOWED_FOR_OWNER_OR_ADMIN)
	public void update(User user) {
		userDao.update(user);
	}

	public User getByLogin(String login) {
		return userDao.getByLogin(login);
	}
}
