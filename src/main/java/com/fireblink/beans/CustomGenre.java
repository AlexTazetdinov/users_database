package com.fireblink.beans;

import com.fireblink.models.Genre;
import com.fireblink.services.GenreService;

public class CustomGenre extends Genre {
public Genre getGenre(GenreService genreService){
	setMovies(genreService.getById(getId()).getMovies());
	return new Genre(this);
}
}
