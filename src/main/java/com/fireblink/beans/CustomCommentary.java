package com.fireblink.beans;

import java.time.LocalDateTime;

import com.fireblink.models.Commentary;
import com.fireblink.services.MovieService;

public class CustomCommentary extends Commentary{

private Integer movieId;
public Commentary getCommentary(MovieService movieService){
	setMovie(movieService.getById(movieId));
	setDateTime(LocalDateTime.now());
	return new Commentary(this);
}
public Integer getMovieId() {
	return movieId;
}
public void setMovieId(Integer movieId) {
	this.movieId = movieId;
}
}
