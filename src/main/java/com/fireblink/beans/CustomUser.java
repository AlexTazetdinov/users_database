package com.fireblink.beans;

import com.fireblink.models.User;
import com.fireblink.services.GroupService;
import com.fireblink.services.UserService;

public class CustomUser extends User {

	private static final int DEFAULT_GROUP_ID = 2;

	public User getUserForCreate(GroupService groupService){
		User user=new User(this);
		if(this.getId()==null)
		user.setGroup(groupService.getById(DEFAULT_GROUP_ID));
		return user;
	}
	
	public User getUserForUpdate(GroupService groupService){
		User user=new User(this);
		user.setGroup(groupService.getByUserId(user.getId()));
		return user;
	}
}
