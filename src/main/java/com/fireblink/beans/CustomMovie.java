package com.fireblink.beans;

import java.io.IOException;
import java.util.Arrays; 
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fireblink.models.Genre;
import com.fireblink.models.Image;
import com.fireblink.models.Movie;
import com.fireblink.services.CommentaryService;
import com.fireblink.services.GenreService;
import com.fireblink.services.MovieService;


public class CustomMovie extends Movie {
private Integer[] genresIdArray;

private MultipartFile customImage;

public Movie getMovie(MovieService movieService,GenreService genreService){
	Set<Genre> genres=new HashSet<Genre>();
	for(Integer genreId:genresIdArray){
		genres.add(genreService.getById(genreId));
	}
	setGenres(genres);
	if(getId()!=null){
		setCommentaries(movieService.getById(getId()).getCommentaries());
		}
	if(customImage!=null)
	try {
		setImage(new Image(customImage.getName(),customImage.getBytes()));
	} catch (IOException e) {
		e.printStackTrace();
	}
	else
		setImage(null);
	return new Movie(this);
}

public Integer[] getGenresArray() {
	return genresIdArray;
}

public void setGenresArray(Integer[] genresArray) {
	this.genresIdArray = genresArray;
}

public Integer[] getGenresIdArray() {
	return genresIdArray;
}

public void setGenresIdArray(Integer[] genresIdArray) {
	this.genresIdArray = genresIdArray;
}

public MultipartFile getCustomImage() {
	return customImage;
}

public void setCustomImage(MultipartFile customImage) {
	this.customImage = customImage;
}
}
