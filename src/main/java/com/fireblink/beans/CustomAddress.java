package com.fireblink.beans;

import org.springframework.beans.factory.annotation.Autowired;

import com.fireblink.models.Address;
import com.fireblink.services.AddressService;
import com.fireblink.services.UserService;

public class CustomAddress extends Address {

	private Integer userId;

	public Address getAddress(AddressService addressService,UserService userService) {
		Address address = new Address(this);
		if(userId==null)
		address.setUser(addressService.getById(getId()).getUser());
		else address.setUser(userService.getById(userId));
		return address;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
}
