package com.fireblink.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "GENRES")
public class Genre {
	private Integer id;
	private String name;
	private Set<Movie> movies;

	public Genre() {

	}

	public Genre(String name) {
		this.name = name;
	}
	
	public Genre(Genre genre){
		this.name=genre.getName();
		this.id=genre.getId();
		this.movies=genre.getMovies();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(unique = true)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany(fetch=FetchType.LAZY,cascade = { CascadeType.PERSIST, CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH})
	@JoinTable(name = "MOVIES_GENRES", joinColumns = @JoinColumn(name = "genre_id", updatable=true,nullable = false), inverseJoinColumns = @JoinColumn(name = "movie_id",updatable=true, nullable = false))
	public Set<Movie> getMovies() {
		return movies;
	}

	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}

}
