package com.fireblink.models;

import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "USERS")
public class User {

	private Integer id;

	private String email;

	private String login;

	private String password;

	private String name;

	private String surname;

	private Set<Address> addresses;

	private Group group;

	public User() {
	}

	public User(User user) {
		super();
		this.id = user.id;
		this.email = user.email;
		this.login = user.login;
		this.password = user.password;
		this.name = user.name;
		this.surname = user.surname;
		this.addresses = user.addresses;
		this.group = user.group;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "email", unique = true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "login", unique = true)
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "surname")
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@OneToMany(mappedBy = "user", cascade=CascadeType.REMOVE, fetch = FetchType.EAGER)
	public Set<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "group_id")
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public boolean hasRole(Role role) {
		return (group.getRoles().contains(role));
	}

	public boolean hasAddressWithId(Integer addressId) {
		return addresses.size() != 0 && (addresses.stream().anyMatch(x -> x.getId() == addressId));
	}

}
