package com.fireblink.models;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MOVIES")
public class Movie {
	private Integer id;
	private String name;
	private Set<Genre> genres;
	private Integer duration;
	private String description;
	private String link;
	private Set<Commentary> commentaries;
	private Image image;

	public Movie() {
	}

	public Movie(Movie movie) {
		this.id = movie.id;
		this.name = movie.name;
		this.genres = movie.genres;
		this.duration = movie.duration;
		this.description = movie.description;
		this.link = movie.link;
		this.commentaries = movie.commentaries;
		this.image=movie.image;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany(fetch=FetchType.LAZY,cascade = { CascadeType.PERSIST, CascadeType.MERGE,CascadeType.DETACH,CascadeType.REFRESH})
	@JoinTable(name = "MOVIES_GENRES", joinColumns = @JoinColumn(name = "movie_id", updatable=true,nullable = false), inverseJoinColumns = @JoinColumn(name = "genre_id",updatable=true, nullable = false))
	public Set<Genre> getGenres() {
		return genres;
	}

	public void setGenres(Set<Genre> genre) {
		this.genres = genre;
	}

	@Column
	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	@Column
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@OneToMany(mappedBy="movie",cascade={CascadeType.REMOVE})
	public Set<Commentary> getCommentaries() {
		return commentaries;
	}

	public void setCommentaries(Set<Commentary> commentaries) {
		this.commentaries = commentaries;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
    @OneToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="image_id", nullable=true)
	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
//	public String generateStringImage(){
//		return Base64.getEncoder().encodeToString(image);
//	}

	public String convertGenresInString() {
		List<String> genreNames = new ArrayList<String>();
		for (Genre genre : genres) {
			genreNames.add(genre.getName());
		}
		return genreNames.stream().collect(Collectors.joining(","));
	}	
}
