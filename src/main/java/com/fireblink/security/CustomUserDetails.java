package com.fireblink.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import com.fireblink.models.Role;
import com.fireblink.models.User;

public class CustomUserDetails extends User implements UserDetails {

	private static final long serialVersionUID = 1L;
	private List<Role> userRoles;

	public CustomUserDetails(User user, List<Role> userRoles) {
		super(user);
		this.userRoles = userRoles;
	}
    public CustomUserDetails(User user){
    	super(user);
    	userRoles=new ArrayList();
    	userRoles.addAll(user.getGroup().getRoles());
    } 
	public boolean hasAuthority(String authority){
		return getAuthorities().contains(new SimpleGrantedAuthority(authority));
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		String roles=StringUtils.collectionToCommaDelimitedString(userRoles);			
		return AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getUsername() {
		return super.getLogin();
	}

}
