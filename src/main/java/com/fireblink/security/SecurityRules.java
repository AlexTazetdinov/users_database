package com.fireblink.security;

public final class SecurityRules {
	public static final String USER_ALLOWED_FOR_OWNER_OR_ADMIN = "principal.getId().equals(#user.getId()) or principal.hasAuthority('ROLE_ADMIN')";
	public static final String ADDRESS_ALLOWED_FOR_OWNER_OR_ADMIN = "principal.getId().equals(#address.getUser().getId())or principal.hasAuthority('ROLE_ADMIN')";
}
