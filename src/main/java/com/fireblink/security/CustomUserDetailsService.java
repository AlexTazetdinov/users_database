package com.fireblink.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import com.fireblink.dao.RoleDao;
import com.fireblink.dao.UserDao;
import com.fireblink.models.Role;
import com.fireblink.models.User;
import com.fireblink.services.RoleService;
import com.fireblink.services.UserService;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

	private final UserService userService;
	private final RoleService roleService;

	@Autowired
	public CustomUserDetailsService(UserService userService,RoleService roleService) {
		this.userService = userService;
		this.roleService=roleService;
	}

	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		User user=userService.getByLogin(login);
		if (user == null) {
			throw new UsernameNotFoundException("No user present with username: " + login);
		} else {
			List<Role> roles = roleService.getRolesByLogin(user.getLogin());
			return new CustomUserDetails(user, roles);
		}
	}
}
