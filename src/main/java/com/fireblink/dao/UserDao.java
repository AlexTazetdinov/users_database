package com.fireblink.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fireblink.models.User;

@Repository
@Transactional
public class UserDao extends AbstractDao<User> {
	
	public UserDao(){
		setSpecifiedClass(User.class);
	}

	public User getByEmail(String email) {
		return (User) getSession().createCriteria(User.class).add(Restrictions.like("email", email)).uniqueResult();
	}

	public User getByLogin(String login) {
		return (User) getSession().createCriteria(User.class).add(Restrictions.like("login", login)).uniqueResult();
	}

}
