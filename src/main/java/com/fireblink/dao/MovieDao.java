package com.fireblink.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fireblink.models.Genre;
import com.fireblink.models.Group;
import com.fireblink.models.Movie;
@Repository
@Transactional
public class MovieDao extends AbstractDao<Movie>{
	MovieDao(){
		setSpecifiedClass(Movie.class);
	} 
}
