package com.fireblink.dao;

import javax.transaction.Transactional;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fireblink.models.Genre;
import com.fireblink.models.Group;

@Repository
@Transactional
public class GenreDao extends AbstractDao<Genre> {
	GenreDao() {
		setSpecifiedClass(Genre.class);
	}

	public Genre getByName(String name) {
		return (Genre) getSession().createCriteria(Genre.class).add(Restrictions.like("name", name)).uniqueResult();
	}	
}
