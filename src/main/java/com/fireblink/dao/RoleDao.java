package com.fireblink.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.fireblink.models.Role;

@Repository
@Transactional
public class RoleDao extends AbstractDao<Role> {

	RoleDao() {
		setSpecifiedClass(Role.class);
	}
}
