package com.fireblink.dao;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;

import com.fireblink.models.User;

@Transactional
public abstract class AbstractDao<T> {
	@PersistenceContext
	EntityManager entityManager;
	private Class specifiedClass;
	
	

	public Class getSpecifiedClass() {
		return specifiedClass;
	}

	public void setSpecifiedClass(Class<T> childClass) {
		this.specifiedClass = childClass;
	}

	public List<T> getAll() {
		return getSession().createCriteria(specifiedClass).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	public void create(T entity) {
		entityManager.persist(entity);
	}

	public void delete(T entity) {
			entityManager.remove(entity);
	}

	public T getById(int id) {
		return  (T)getSession().createCriteria(specifiedClass).add(Restrictions.like("id", id)).uniqueResult();
	}

	public void update(T entity) {
		entityManager.merge(entity);
	}


	protected Session getSession() {
		Session session = entityManager.unwrap(Session.class);
		return session;
	}
	}
