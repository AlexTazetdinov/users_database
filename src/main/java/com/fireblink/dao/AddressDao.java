package com.fireblink.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.fireblink.models.Address;
import com.fireblink.models.User;

@Repository
@Transactional
public class AddressDao extends AbstractDao<Address> {
	public AddressDao(){
		setSpecifiedClass(Address.class);
	}
}
