package com.fireblink.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fireblink.models.Group;
import com.fireblink.models.Image;

@Repository
@Transactional
public class ImageDao extends AbstractDao<Image> {
	ImageDao(){
		setSpecifiedClass(Image.class);
	}

}
