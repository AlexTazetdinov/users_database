package com.fireblink.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fireblink.models.Commentary;

@Repository
@Transactional
public class CommentaryDao extends AbstractDao<Commentary> {
	CommentaryDao(){
		setSpecifiedClass(Commentary.class);
	}

}
