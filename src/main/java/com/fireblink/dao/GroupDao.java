package com.fireblink.dao;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.fireblink.models.Group;

@Repository
@Transactional
public class GroupDao extends AbstractDao<Group>{
	GroupDao(){
		setSpecifiedClass(Group.class);
	}
}
