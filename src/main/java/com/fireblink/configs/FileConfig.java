package com.fireblink.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;


//@Configuration
//@ComponentScan({ "com.fireblink" })
public class FileConfig {
    @Bean(name="multipartResolver")
    public MultipartResolver resolver(){
        return new CommonsMultipartResolver();
    }
}
