<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<body>
<div class="header">
<button id="btnGoToMoviesTab"class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-arrow-left"></span></button>
<div class="greeting text-success">
		<spring:message code="welcome.greeting" />
		${pageContext.request.userPrincipal.name}
	</div>
	<div class="language">
		<select class="language" id="languages">
			<option value="en">en</option>
			<option value="ru">ru</option>
		</select>
	</div>
	<div class="logout">
		<a href="/logout"> <span class="glyphicon glyphicon-log-out"></span>
		</a>
	</div>
	</div>
<div class="title">
Movie "${movie.getName()}"  
</div>
<div class="movieDetails">
<img class="movieImage" src="/image/<c:out value='${movie.getImage().getId()}'/>" />
</div>
<div class="movieDetails">
<p>
<b>Genres:</b> ${movie.convertGenresInString()}
</p>
<p>
<b>Link:</b> ${movie.getLink()}
</p>
</div>

<c:forEach items="${comments}" var="comment">
<div class="comment">
<div class="comment-header">
<p>Post: ${comment.getUser().getLogin()}  ${comment.getDateTime().toLocalDate()} ${comment.getDateTime().toLocalTime()}</p>
</div>
<div class="comment-filling">
<p>
${comment.getText()}
</p>
</div>
</div>
</c:forEach>
</div>
<textarea id="commentText" class="form-control add-comment">
</textarea>
<div class="btn-add-comment">
<button id="btnAddComment" class="btn btn-primary">
Add comment
</button>
</div>
<input type="hidden" id="movieIdInComments" value="${movie.getId()}">
</body>