<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<c:forEach items="${genres}" var="genre">
<option value="${genre.getId()}">${genre.getName()}</option>
</c:forEach>
