<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<tbody>
	<tr>
		<th></th>
		<th><spring:message code="table.addresses.description" /></th>
		<th><spring:message code="table.addresses.country" /></th>
		<th><spring:message code="table.addresses.city" /></th>
		<th><spring:message code="table.addresses.region" /></th>
		<th><spring:message code="table.addresses.street" /></th>
		<th><spring:message code="table.addresses.house" /></th>
		<th><spring:message code="table.addresses.flat" /></th>
		<th><spring:message code="table.addresses.zip" /></th>
		<c:forEach items="${addresses}" var="address">
			<tr>
				<td><input type="radio" value="${address.getId()}"
					name="checked-address"></td>
				<td id="description${address.getId()}">${address.getDescription()}</td>
				<td id="country${address.getId()}">${address.getCountry()}</td>
				<td id="city${address.getId()}">${address.getCity()}</td>
				<td id="region${address.getId()}">${address.getRegion()}</td>
				<td id="street${address.getId()}">${address.getStreet()}</td>
				<td id="house${address.getId()}">${address.getHouse()}</td>
				<td id="flat${address.getId()}">${address.getFlat()}</td>
				<td id="zip${address.getId()}">${address.getZip()}</td>
			</tr>
		</c:forEach>
		<input type="hidden" id="userId"
			<c:if test="${addresses.size()>0}">
					value="${addresses.get(0).getUser().getId()}"
					</c:if>>
</tbody>