<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<tbody>
	<tr>
		<th></th>
		<th>Name</th>
		<c:forEach items="${genres}" var="genre">
			<tr>
				<td width="5%"><input type="radio" value="${genre.getId()}"
					name="checked-genre"></td>
				<td id="genreName${genre.getId()}">${genre.getName()}</td>
			</tr>
		</c:forEach>
</tbody>