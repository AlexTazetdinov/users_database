
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<tbody>
	<tr>
		<th></th>
		<th><spring:message code="table.users.login"/></th>
		<th><spring:message code="table.users.password"/></th>
		<th><spring:message code="table.users.email"/></th>
		<th><spring:message code="table.users.name"/></th>
		<th><spring:message code="table.users.surname"/></th>
		<th>Add-s</th>
		<th>Group</th>
		<th>Roles</th>
		<c:forEach items="${users}" var="user">
			<tr>
				<td><input type="radio" value="${user.getId()}"
					name="checked-user"></td>
				<td id="login${user.getId()}">${user.getLogin()}</td>
				<td id="password${user.getId()}">${user.getPassword()}</td>
				<td id="email${user.getId()}">${user.getEmail()}</td>
				<td id="name${user.getId()}">${user.getName()}</td>
				<td id="surname${user.getId()}">${user.getSurname()}</td>
				<td><button id="${user.getId()}" class="button-as-link addresses-for-single-user-link">${user.getAddresses().size()}</button></td>
				<td>${user.getGroup().getName()}</td>
				<td><c:out value="${roles[user.getId()]}"/></td>
			</tr>
		</c:forEach>
</tbody>


