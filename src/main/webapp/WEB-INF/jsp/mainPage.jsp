<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<html lang="en">
<head>
<meta charset="utf-8">
<link href="<c:url value="css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="css/bootstrap-select.css" />" rel="stylesheet">
<link href="<c:url value="css/main.css" />" rel="stylesheet">
<link href="<c:url value="css/user-table.css" />" rel="stylesheet">
<link href="<c:url value="css/icons.css" />" rel="stylesheet">
<link href="<c:url value="css/comment.css" />" rel="stylesheet">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<c:url value="js/bootstrap.js" />"></script>
<script src="<c:url value="js/userTablePage.js" />"></script>
<script src="<c:url value="js/request-helper.js" />"></script>
<script src="<c:url value="js/bootstrap-select.js" />"></script>

</head>
<body>
	<ul class="nav nav-tabs" id="tabs">
		<li class="active"><a data-toggle="tab" href="#users"
			id="usersTab"><spring:message code="tab.user" /></a></li>
		<li><a data-toggle="tab" href="#addresses" id="addressTab">Addresses</a></li>
		<li><a data-toggle="tab" href="#movies" id="moviesTab">Movies</a></li>
		<li><a data-toggle="tab" href="#genres" id="genresTab">Genres</a></li>
	</ul>
	<div class="header">
		<div class="greeting text-success">
			<spring:message code="welcome.greeting" />
			${pageContext.request.userPrincipal.name}
		</div>
		<div class="language">
			<select class="language" id="languages">
				<option value="en">en</option>
				<option value="ru">ru</option>
			</select>
		</div>
		<div class="logout">
			<a href="/logout"> <span class="glyphicon glyphicon-log-out"></span>
			</a>
		</div>
	</div>
	<div class="tab-content">
		<div id="users" class="tab-pane fade in active">
			<h4 class="text-center text-primary">
				<spring:message code="table.users.title" />
			</h4>
			<table id="userTable"
				class="user-table table table-hover table-striped">
			</table>
			<div style="text-align: center;">
				<sec:authorize access="hasRole('ADMIN')">
					<button id="buttonOpenAddUser" type="button"
						class="btn btn-table btn-primary ">
						<spring:message code="button.add" />
					</button>
				</sec:authorize>

				<button id=buttonOpenUpdateUser type="button"
					class="btn btn-table btn-primary">
					<spring:message code="button.update" />
				</button>

				<button id="buttonDeleteUser" type="button"
					class="btn btn-table btn-primary">
					<spring:message code="button.delete" />
				</button>
				<a id="buttonOpenSendEmail" class="btn  btn-table btn-lg""> <span
					class="glyphicon glyphicon-envelope"></span>
				</a>

			</div>
		</div>



		<div id="addresses" class="tab-pane fade">
			<h4 class="text-center text-primary">
				<spring:message code="table.addresses.title" />
			</h4>
			<table id="addressTable"
				class="user-table table table-hover table-striped">
			</table>
			<div style="text-align: center;">
				<button id="buttonOpenAddAddress" type="button"
					class="btn btn-table btn-primary ">
					<spring:message code="button.add" />
				</button>

				<button id="buttonOpenUpdateAddress" type="button"
					class="btn btn-table btn-primary">
					<spring:message code="button.update" />
				</button>

				<button id="buttonDeleteAddress" type="button"
					class="btn btn-table btn-primary">
					<spring:message code="button.delete" />
				</button>
			</div>
		</div>

		<div id="movies" class="tab-pane fade">
			<h4 class="text-center text-primary">Movies</h4>
			<table class="user-table table table-hover table-striped"
				id="moviesTable">
			</table>
			<div style="text-align: center;">
				<sec:authorize access="hasRole('ADMIN')">
					<button id="buttonOpenAddMovie" type="button"
						class="btn btn-table btn-primary ">
						<spring:message code="button.add" />
					</button>

					<button id="buttonOpenUpdateMovie" type="button"
						class="btn btn-table btn-primary ">
						<spring:message code="button.update" />
					</button>

					<button id="buttonDeleteMovie" type="button"
						class="btn btn-table btn-primary">
						<spring:message code="button.delete" />
					</button>
				</sec:authorize>

			</div>
		</div>
		<div id="genres" class="tab-pane fade">
			<h4 class="text-center text-primary">Genres</h4>
			<table class="user-table table table-hover table-striped"
				id="genresTable">
			</table>
			<div style="text-align: center;">
				<sec:authorize access="hasRole('ADMIN')">
					<button id="buttonOpenAddGenre" type="button"
						class="btn btn-table btn-primary ">
						<spring:message code="button.add" />
					</button>
					<button id="buttonOpenUpdateGenre" type="button"
						class="btn btn-table btn-primary">
						<spring:message code="button.update" />
					</button>
					<button id="buttonDeleteGenre" type="button"
						class="btn btn-table btn-primary">
						<spring:message code="button.delete" />
					</button>
				</sec:authorize>

			</div>
		</div>
	</div>
	<div id="modalCreateUser" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="text-center">
						<spring:message code="add.title" />
					</h2>
				</div>
				<div class="modal-body">
					<div id="AddUser">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<p>
										<spring:message code="table.users.login" />
									</p>
									<p>
										<spring:message code="table.users.password" />
									</p>
									<p>
										<spring:message code="table.users.email" />
									</p>
									<p>
										<spring:message code="table.users.name" />
									</p>
									<p>
										<spring:message code="table.users.surname" />
									</p>
								</div>
								<div class="col-md-2  form-group form-group-sm">

									<p>
										<input type="text" class="form-control" id="login">
									</p>
									<p>
										<input type="text" class="form-control" id="password">
									</p>
									<p>
										<input type="text" class="form-control" id="email">
									<p>
										<input type="text" class="form-control" id="name">
									</p>
									<p>
										<input type="text" class="form-control" id="surname">
									</p>

								</div>
							</div>
						</div>
						<div class="text-center">
							<button id="buttonAddUser" class="btn btn-primary">
								<spring:message code="add.button" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalUpdateUser" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="text-center">
						<spring:message code="update.title" />
					</h2>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<p>
									<spring:message code="table.users.login" />
								</p>
								<p>
									<spring:message code="table.users.password" />
								</p>
								<p>
									<spring:message code="table.users.email" />
								</p>
								<p>
									<spring:message code="table.users.name" />
								</p>
								<p>
									<spring:message code="table.users.surname" />
								</p>
							</div>
							<div class="col-md-2  form-group form-group-sm">
								<p>
									<input type="text" class="form-control" id="loginUpdate">
								</p>
								<p>
									<input type="text" class="form-control" id="passwordUpdate">
								</p>
								<p>
									<input type="text" class="form-control" id="emailUpdate">
								</p>
								<p>
									<input type="text" class="form-control" id="nameUpdate">
								</p>
								<p>
									<input type="text" class="form-control" id="surnameUpdate">
								</p>
							</div>
						</div>
					</div>
					<div class="text-center">
						<button id="buttonUpdateUser" class="btn btn-primary">
							<spring:message code="update.button" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalError" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p>
						<spring:message code="error.selection" />
					</p>
				</div>
			</div>
		</div>
	</div>
	<div id="modalSendEmail" class="modal" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div id="AddUser">
						<div class="col-md-12  form-group form-group-sm">
							<form id="loginForm">
								<p>
									<label for="subject"><spring:message
											code="mail.subject" /></label> <input type="text"
										class="form-control" id="subject" name="subject">
								</p>
								<p>
								<div class="form-group">
									<label for="message"><spring:message
											code="mail.message" /></label>
									<textarea class="form-control" rows="10" id="text"></textarea>
								</div>

							</form>
						</div>
					</div>
					<div class="text-center">
						<button id="btnadd" class="btn btn-primary">
							<spring:message code="mail.button" />
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="modalCreateAddress" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="text-center">Add address</h2>
				</div>
				<div class="modal-body">
					<div id="AddAddress">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<p>
										<spring:message code="table.addresses.description" />
									</p>
									<p>
										<spring:message code="table.addresses.country" />
									</p>
									<p>
										<spring:message code="table.addresses.region" />
									</p>
									<p>
										<spring:message code="table.addresses.city" />
									</p>
									<p>
										<spring:message code="table.addresses.street" />
									</p>
									<p>
										<spring:message code="table.addresses.house" />
									</p>
									<p>
										<spring:message code="table.addresses.flat" />
									</p>
									<p>
										<spring:message code="table.addresses.zip" />
									</p>
									<p>User</p>
								</div>
								<div class="col-md-2  form-group form-group-sm">

									<p>
										<input type="text" class="form-control" id="description">
									</p>
									<p>
										<input type="text" class="form-control" id="country">
									</p>
									<p>
										<input type="text" class="form-control" id="region">
									<p>
										<input type="text" class="form-control" id="city">
									</p>
									<p>
										<input type="text" class="form-control" id="street">
									</p>
									<p>
										<input type="text" class="form-control" id="house">
									</p>
									<p>
										<input type="text" class="form-control" id="flat">
									</p>
									<p>
										<input type="text" class="form-control" id="zip">
									</p>
									<p>
										<select class="selectUser" id="addUserSelector">
										</select>
									</p>

								</div>
							</div>
						</div>
						<div class="text-center">
							<button id="buttonAddAddress" class="btn btn-primary">
								<spring:message code="add.button" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalUpdateAddress" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="text-center">Update address</h2>
				</div>
				<div class="modal-body">
					<div id="UpdateAddress">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<p>
										<spring:message code="table.addresses.description" />
									</p>
									<p>
										<spring:message code="table.addresses.country" />
									</p>
									<p>
										<spring:message code="table.addresses.region" />
									</p>
									<p>
										<spring:message code="table.addresses.city" />
									</p>
									<p>
										<spring:message code="table.addresses.street" />
									</p>
									<p>
										<spring:message code="table.addresses.house" />
									</p>
									<p>
										<spring:message code="table.addresses.flat" />
									</p>
									<p>
										<spring:message code="table.addresses.zip" />
									</p>
								</div>
								<div class="col-md-2  form-group form-group-sm">

									<p>
										<input type="text" class="form-control" id="descriptionUpdate">
									</p>
									<p>
										<input type="text" class="form-control" id="countryUpdate">
									</p>
									<p>
										<input type="text" class="form-control" id="regionUpdate">
									<p>
										<input type="text" class="form-control" id="cityUpdate">
									</p>
									<p>
										<input type="text" class="form-control" id="streetUpdate">
									</p>
									<p>
										<input type="text" class="form-control" id="houseUpdate">
									</p>
									<p>
										<input type="text" class="form-control" id="flatUpdate">
									</p>
									<p>
										<input type="text" class="form-control" id="zipUpdate">
									</p>
									<p></p>

								</div>
							</div>
						</div>
						<div class="text-center">
							<button id="buttonUpdateAddress" class="btn btn-primary">
								<spring:message code="update.button" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalAddMovie" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="text-center">Add movie</h2>
				</div>
				<div class="modal-body">
					<div id="AddMovie">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<p>name</p>
									<p>link</p>
									<p>genres</p>
									<p>file</p>
								</div>
								<div class="col-md-2  form-group form-group-sm">

									<p>
										<input type="text" class="form-control" id="movieNameAdd">
									</p>
									<p>
										<input type="text" class="form-control" id="movieLinkAdd">
									</p>
									<p>
										<select id="movieGenresSelectorAdd"
											class="selectpicker movieGenresSelector" title="Choose genre"
											multiple data-size="5" data-width="100%" autocomplete="off">
										</select>
									</p>
									<p>
										<label class="btn btn-success btn-file"> Upload
											image<input style="display:none;" type="file" "
											id="moviePictureAdd" accept="image/*">
										</label>
									</p>


								</div>
							</div>
						</div>
						<div class="text-center">
							<button id="buttonAddMovie" class="btn btn-primary">
								<spring:message code="add.button" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="modalUpdateMovie" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="text-center">Update movie</h2>
				</div>
				<div class="modal-body">
					<div id="UpdateMovie">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<p>name</p>
									<p>link</p>
									<p>genres</p>
									<p>file</p>
								</div>
								<div class="col-md-2  form-group form-group-sm">

									<p>
										<input type="text" class="form-control" id="movieNameUpdate">
									</p>
									<p>
										<input type="text" class="form-control" id="movieLinkUpdate">
									</p>
									<p>
									<select id="movieGenresSelectorUpdate"
										class="selectpicker movieGenresSelector" title="Choose genre"
										multiple data-size="5" data-width="100%" autocomplete="off">
									</select>
									</p>
									<p>
										<label class="btn btn-success btn-file"> Upload
											image<input style="display:none;" type="file" "
											id="moviePictureUpdate" accept="image/*">
										</label>
									</p>
									

								</div>
								
							</div>
						</div>
						<div class="text-center">
							<button id="buttonUpdateMovie" class="btn btn-primary">
								<spring:message code="update.button" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalAddGenre" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="text-center">
						<spring:message code="add.title" />
					</h2>
				</div>
				<div class="modal-body">
					<div id="AddGenre">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<p>name</p>
								</div>
								<div class="col-md-2  form-group form-group-sm">
									<p>
										<input type="text" class="form-control" id="addGenreName">
									</p>
								</div>
							</div>
						</div>
						<div class="text-center">
							<button id="buttonAddGenre" class="btn btn-primary">
								<spring:message code="add.button" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalUpdateGenre" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h2 class="text-center">Update genre</h2>
				</div>
				<div class="modal-body">
					<div id="updateGenre">
						<div class="container">
							<div class="row">
								<div class="col-md-4">
									<p>name</p>
								</div>
								<div class="col-md-2  form-group form-group-sm">
									<p>
										<input type="text" class="form-control" id="genreNameUpdate">
									</p>
								</div>
							</div>
						</div>
						<div class="text-center">
							<button id="buttonUpdateGenre" class="btn btn-primary">
								<spring:message code="update.button" />
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modalNoAccess" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<p>No rights for this action.</p>
				</div>
			</div>
		</div>
	</div>
	<p hidden="true" id="emptyFieldError">
		<spring:message code="error.emptyField" />
	</p>
	<p hidden="true" id="existedError">
		<spring:message code="error.existed" />
	</p>
	<input type=hidden id=userEmail
		value="${pageContext.request.userPrincipal.name}" />
	<input type=hidden id=locale value="${pageContext.response.locale}" />

	<footer class="text-center text-primary navbar-fixed-bottom">
		©Alex Tazetdinov </footer>
	<script type="text/javascript">
		main();
	</script>
</body>
</html>