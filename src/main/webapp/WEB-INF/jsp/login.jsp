<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html lang="en">
<head>
<meta charset="utf-8">
<link href="<c:url value="css/bootstrap.css" />" rel="stylesheet">
<link href="<c:url value="css/login.css" />" rel="stylesheet">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script src="<c:url value="js/bootstrap.js" />"></script>
<script src="<c:url value="js/loginPage.js" />"></script>
</head>
<body>
	<div id="modalLogin" class="modal" role="dialog">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
					<div id="AddUser">
						<div class="col-md-12  form-group form-group-sm">
							<form id="loginForm">						
									<div class="text-danger" id="error"></div>
								<p>
									<input type="text" class="form-control" id="login" name="login"
										placeholder=<spring:message code="auth.login"/>>
								</p>
								<p>
									<input type="text" class="form-control" id="password"
										name="password" placeholder=<spring:message code="auth.password"/>>
								</p>
							</form>
						</div>
					</div>
					<div class="text-center">
						<button id="btnadd" onclick="sendLoginRequest()"
							class="btn btn-primary"><spring:message code="auth.submit"/></button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		show();
	</script>
</body>