<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html;charset=UTF-8"%>

<tbody>
	<tr>
		<th></th>
		<th>Name</th>
		<th>Genres</th>
		<th>Link</th>
		<th>Comments</th>
		<c:forEach items="${movies}" var="movie">
			<tr>
				<td><input type="radio" value="${movie.getId()}"
					name="checked-movie"></td>
				<td id="movieName${movie.getId()}">${movie.getName()}</td>
				<td id="movieGenre${movie.getId()}">${movie.convertGenresInString()}</td>
				<td id="movieLink${movie.getId()}">${movie.getLink()}</td>
				<td><button id="${movie.getId()}"
						class="button-as-link comments-for-movie">${movie.getCommentaries().size()}</button></td>
			</tr>
		</c:forEach>
</tbody>
