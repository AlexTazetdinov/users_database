package com.fireblink.services;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.fireblink.configs.DatabaseConfig;
import com.fireblink.models.Role;
import com.fireblink.models.User;
import com.fireblink.services.UserService;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:db.properties")
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration
@SpringBootTest
public class UserServiceTest {

	@Autowired
	private UserService userService;

	private User createSimpleUser() {
		User user = new User();
		user.setLogin("Alexandr");
		user.setPassword("456");
		user.setEmail("alexandr@cc.com");
		user.setName("Alexandr");
		user.setSurname("Alexandrov");
		return user;
	}

	@Test
	public void testForEmptyUserTable() throws Exception {
		assertThat(userService.getAll().size(), is(0));
	}

	@Test
	public void testForCreatingAndGettingUser() throws Exception {
		User user=createSimpleUser();
		userService.add(user);
		User userInDb=userService.getById(1);
		assertThat(userInDb, is(user));
	}
	
	@Test
	public void testForChangingUser() throws Exception {
		User newUser = createSimpleUser();
		userService.add(newUser);
		newUser.setId(1);
		newUser.setLogin("Al");
		userService.update(newUser);
		assertThat(userService.getById(1),is(newUser));
	}
}
